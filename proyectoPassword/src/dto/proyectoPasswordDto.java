package dto;

import javax.swing.JOptionPane;

public class proyectoPasswordDto {
	
	//Aqu� creo las variables tipo private para configurar los constructores
	private int longitud;
	private String password;
	
	
	//Aqu� creo un constructor donde password coge como valor un m�todo creado posteriormente
	public proyectoPasswordDto(int longitud, String password) {
		super();
		this.longitud = longitud;
		this.password = generarContrasenya(longitud);
	}
	
	
	
	//Aqu� creo un m�todo que genera una contrasenya usando codigo ascii aleatorio
	private static String generarContrasenya(int longitud) {
		
		
		char[] array = new char[longitud];
		
		for (int i = 0; i < longitud; i++) {
			
			char numeroAleatorio = (char) (Math.random() * (122 - 48 + 1) + 48);
			array[i] = numeroAleatorio;
		}
		String charString = new String(array);

		return charString;
	}
	
	//Aqu� transformo los constructores a string
	@Override
	public String toString() {
		return "proyectoPasswordDto [longitud=" + longitud + ", password=" + password + "]";
	}
	

}


